import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
        id: 1,
        email: 'ZhangHao@gmail.com',
        password: 'Pass@Word123',
        fullName: 'Zhang Hao',
        gender: 'male',
        roles: ['user']
    })

    return { currentUser }
})